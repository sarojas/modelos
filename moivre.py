import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from math import factorial as fac
import random
from math import sqrt


def binomial(x, y):
    try:
        binom = fac(x) // fac(y) // fac(x - y)
    except ValueError:
        binom = 0
    return binom

def  bin_distribution(n,p, k):
    binom = binomial(n, k)
    dis = binom * pow(p,k) * pow(1 - p, n - k)
    return dis

#######
###########

p = 0.7                                                     ## n y  p se modifican para generar los distintos gráficos
n = 50

print("n: ", n,"|np: ", n*p, "|nq: ", n*(1 - p))
if n >10 and n*p > 5 and n*(1 - p) > 5:
    arr = random.sample(range(0,n), n)
    arr = sorted(arr)
    binom = []
    for i in range(0, len(arr)):
        binom.append(bin_distribution(n, p, arr[i]))

    plt.plot(arr, binom, 'ro')                              #hasta aqui grafica distribucion binomial de una muestra al azar

    mean = n * p
    std = sqrt(n * p * (1 - p))
    normal = stats.norm.pdf(arr, mean, std)
    plt.plot(arr, normal)

##################
#################


plt.show()
